package com.volansystech.roomdatabase.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.volansystech.roomdatabase.R;
import com.volansystech.roomdatabase.dbmodel.University;

import java.util.List;

/**
 * Created by rajeshjadi on 16/2/18.
 */

public class UniversityAdapter extends BaseAdapter {
    Context context;
    List<University> list;

    public UniversityAdapter(Context context, List<University> universities) {
        this.context = context;
        this.list = universities;
    }

    public void updateData(University university1) {
        for (University university : list) {
            if (university.getSlNo() == university1.getSlNo()) {
                notifyDataSetChanged();
            }
        }
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.listitem, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        University currentItem = (University) getItem(position);
        viewHolder.txtId.setText("" + currentItem.getSlNo());
        viewHolder.txtUniversityName.setText(currentItem.getName());
        viewHolder.txtCollegeId.setText("" + currentItem.getCollege().getId());
        viewHolder.txtCollageName.setText(currentItem.getCollege().getName());

        return convertView;
    }

    private class ViewHolder {
        TextView txtId, txtCollegeId;
        TextView txtUniversityName, txtCollageName;

        public ViewHolder(View view) {
            txtId = view.findViewById(R.id.txtId);
            txtUniversityName = view.findViewById(R.id.txtUniversityName);
            txtCollegeId = view.findViewById(R.id.txtCollegeId);
            txtCollageName = view.findViewById(R.id.txtCollageName);
        }
    }
}
