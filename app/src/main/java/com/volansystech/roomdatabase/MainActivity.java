package com.volansystech.roomdatabase;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.volansystech.roomdatabase.adapters.UniversityAdapter;
import com.volansystech.roomdatabase.database.SampleDatabase;
import com.volansystech.roomdatabase.dbmodel.College;
import com.volansystech.roomdatabase.dbmodel.University;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {
    private EditText edtUniversity, edtRoll, edtCollege;
    private Button btnSubmit;
    ListView listView;
    List<University> list = new ArrayList<>();
    UniversityAdapter universityAdapter;
    University university = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        bindData();


    }

    private void initView() {
        edtUniversity = findViewById(R.id.edtUniversity);
        edtRoll = findViewById(R.id.edtRoll);
        edtCollege = findViewById(R.id.edtCollege);
        btnSubmit = findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(this);
        listView = findViewById(R.id.listView);
    }

    private void bindData() {
        list = SampleDatabase.getInstance(this).userDao().fetchAllData();
        universityAdapter = new UniversityAdapter(this, list);
        listView.setAdapter(universityAdapter);
        listView.setOnItemClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnSubmit) {
            if (TextUtils.isEmpty(edtUniversity.getText().toString())) {
                edtUniversity.setError(getString(R.string.please_fill));
            } else if (TextUtils.isEmpty(edtRoll.getText().toString())) {
                edtRoll.setError(getString(R.string.please_fill));
            } else if (TextUtils.isEmpty(edtCollege.getText().toString())) {
                edtCollege.setError(getString(R.string.please_fill));
            } else {
                saveData(university);
            }
        }
    }

    private void saveData(University universityId) {

        if (universityId == null) {
            university = new University();
            university.setName(edtUniversity.getText().toString());
            university.setCollege(new College(Integer.parseInt(edtRoll.getText().toString()), edtCollege.getText().toString()));
            SampleDatabase.getInstance(this).userDao().insertOnlySingleRecord(university);
            edtUniversity.setText("");
            edtCollege.setText("");
            edtRoll.setText("");
            list = SampleDatabase.getInstance(this).userDao().fetchAllData();
            universityAdapter = new UniversityAdapter(this, list);
            listView.setAdapter(universityAdapter);
            university = null;
        } else {
            university.setName(edtUniversity.getText().toString());
            university.setCollege(new College(Integer.parseInt(edtRoll.getText().toString()), edtCollege.getText().toString()));
            SampleDatabase.getInstance(this).userDao().updateRecord(university);
            edtUniversity.setText("");
            edtCollege.setText("");
            edtRoll.setText("");
            universityAdapter.updateData(university);
            university = null;
        }

    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
        if (list != null) {
            AlertDialog.Builder builder;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = new AlertDialog.Builder(MainActivity.this, android.R.style.Theme_Material_Dialog_Alert);
            } else {
                builder = new AlertDialog.Builder(MainActivity.this);
            }
            builder.setTitle(R.string.update_list)
                    .setMessage(R.string.dialog_title)
                    .setPositiveButton(getString(R.string.update), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            university = list.get(position);
                            edtUniversity.setText(university.getName());
                            edtRoll.setText("" + university.getCollege().getId());
                            edtCollege.setText("" + university.getCollege().getName());
                        }
                    })
                    .setNegativeButton(getString(R.string.delete), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            SampleDatabase.getInstance(MainActivity.this).userDao().deleteRecord(list.get(position));
                            list.remove(list.get(position));
                            universityAdapter.notifyDataSetChanged();
                            university = null;
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }
    }
}
