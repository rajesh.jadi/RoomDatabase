package com.volansystech.roomdatabase.dbmodel;

/**
 * Created by rajeshjadi on 16/2/18.
 */

public class College {
    private int id;
    private String name;

    public College(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
