package com.volansystech.roomdatabase.dbmodel;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by rajeshjadi on 16/2/18.
 */
@Entity
public class University {

    @PrimaryKey(autoGenerate = true)
    private int slNo;
    private String name;

    /**
     * Embedded annotation is used for the
     * if same parameter is exits in child class then it will be create column name prefix.
     * e.g clg_name
     */
    @Embedded(prefix = "clg")
    College college;

    public int getSlNo() {
        return slNo;
    }

    public void setSlNo(int slNo) {
        this.slNo = slNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public College getCollege() {
        return college;
    }

    public void setCollege(College college) {
        this.college = college;
    }
}
