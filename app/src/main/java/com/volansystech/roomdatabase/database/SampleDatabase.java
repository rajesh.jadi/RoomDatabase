package com.volansystech.roomdatabase.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.volansystech.roomdatabase.dbmodel.University;

/**
 * Created by rajeshjadi on 16/2/18.
 */

@Database(entities = {University.class}, version = 1)
public abstract class SampleDatabase extends RoomDatabase {
    private static SampleDatabase INSTANCE;

    public abstract DaoAccess userDao();

    /**
     * Single instance of database object
     * @param context
     * @return
     */
    public static SampleDatabase getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), SampleDatabase.class, "user-database")
                            // allow queries on the main thread.
                            // Don't do this on a real app! See PersistenceBasicSample for an example.
                            .allowMainThreadQueries()
                            .build();
        }
        return INSTANCE;
    }

    /**
     * destroy the instance of database.
     */
    public static void destroyInstance() {
        INSTANCE = null;
    }
}
