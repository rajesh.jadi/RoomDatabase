package com.volansystech.roomdatabase.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.volansystech.roomdatabase.dbmodel.University;

import java.util.List;

/**
 * Created by rajeshjadi on 16/2/18.
 */
@Dao
public interface DaoAccess {
    /**
     * Insert multiple records.
     * @param universities
     */
    @Insert
    void insertMultipleRecord(University... universities);

    /**
     * insert multiple records as list.
     * @param universities
     */
    @Insert
    void insertMultipleListRecord(List<University> universities);

    /**
     * Insert single records.
     * @param university
     * @return
     */
    @Insert
    long insertOnlySingleRecord(University university);

    /**
     * fetch all data from table.
     * @return
     */
    @Query("SELECT * FROM University")
    List<University> fetchAllData();

    /**
     * fetch single record by id.
     * @param college_id
     * @return
     */
    @Query("SELECT * FROM University WHERE clgid =:college_id")
    University getSingleRecord(int college_id);

    /**
     * Update Single object.
     * @param university
     * @return
     */
    @Update
    int updateRecord(University university);

    /**
     * delete single records.
     * @param university
     */
    @Delete
    void deleteRecord(University university);
}
